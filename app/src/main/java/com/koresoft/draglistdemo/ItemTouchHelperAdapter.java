package com.koresoft.draglistdemo;

/**
 * Created by Hanut on 14-03-2017.
 */

public interface ItemTouchHelperAdapter {

    void onItemMove(int fromPosition, int toPosition);

    void onItemDismiss(int position);
}
