package com.koresoft.draglistdemo;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.util.ArrayList;

public class MainActivity extends AppCompatActivity {

    RecyclerView dragList;
    LinearLayout dropZone;
    TextView muteZone;
    TextView cancelZone;
    TextView removeZone;
    static MyListAdapter adapter;
    MyDragEventListener listener;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        findViewById(R.id.fab).setVisibility(View.GONE);

        dropZone = (LinearLayout)findViewById(R.id.dropZone);
        muteZone = (TextView)findViewById(R.id.drop_mute);
        cancelZone = (TextView)findViewById(R.id.drop_cancel);
        removeZone = (TextView)findViewById(R.id.drop_remove);

        listener = new MyDragEventListener(this, new MyDragEventListener.DragComplete() {
            private static final String ACTION_MUTE = "MUTE";
            private static final String ACTION_DROP = "DROP";
            private static final String ACTION_CANCEL = "CANCEL";

            @Override
            public void callback(int position,String action) {
                switch (action){
                    case ACTION_DROP:
                        MainActivity.this.adapter.removeItem(position);
                        MainActivity.this.dragList.invalidate();
                        break;
                    case ACTION_MUTE:
                        //TODO MUTE THE FUCKER
                        MainActivity.this.adapter.mute(position);
                        MainActivity.this.dragList.invalidate();
                        break;
                    default:
                        break;
                }
            }
        });

        cancelZone.setOnDragListener(listener);
        removeZone.setOnDragListener(listener);
        muteZone.setOnDragListener(listener);

        dragList = (RecyclerView)findViewById(R.id.dragList);
        ArrayList<String> names = new ArrayList<String>();
        names.add("Hanut");
        names.add("Aditya");
        names.add("Sumantu");
        names.add("Monis");
        names.add("Giric");
        names.add("Vegeta");
        names.add("Goku");
        names.add("Gohan");
        names.add("Robert");
        names.add("Teelu");
        names.add("Bingaa");
        names.add("Bali ka Bakra");
        names.add("Sasha");
        names.add("Vishnu");
        names.add("Vladimir");
        names.add("Boris");
        names.add("Sergei");
        adapter = new MyListAdapter(this,names,listener);
        dragList.setAdapter(adapter);
        dragList.setLayoutManager(new LinearLayoutManager(this));
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }
        return super.onOptionsItemSelected(item);
    }
}
