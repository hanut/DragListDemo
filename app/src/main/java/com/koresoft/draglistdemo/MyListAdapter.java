package com.koresoft.draglistdemo;

import android.app.Activity;
import android.content.ClipData;
import android.graphics.Color;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * Created by Hanut on 14-03-2017.
 */

public class MyListAdapter extends RecyclerView.Adapter<MyViewHolder> implements ItemTouchHelperAdapter{

    private final List<String> mItems = new ArrayList<>();
    private static View.OnLongClickListener listener;
    private static MyDragEventListener dragListener;
    private static Activity activity;

    public MyListAdapter(final Activity activity, List<String> names,MyDragEventListener dragListener) {
        mItems.addAll(names);
        this.activity = activity;
        this.dragListener = dragListener;
        listener = new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View v) {
                MyViewHolder holder = (MyViewHolder) v.getTag();
                int position = holder.position;
                String name = mItems.get(position);
                RelativeLayout layout = (RelativeLayout)v;
                View.DragShadowBuilder builder = new View.DragShadowBuilder(v);
                ClipData item = ClipData.newPlainText(name,String.valueOf(position));
                layout.startDrag(item, builder,null,0);
                layout.setOnDragListener(MyListAdapter.this.dragListener);
                activity.findViewById(R.id.dragList).setVisibility(View.GONE);
                activity.findViewById(R.id.dropZone).setVisibility(View.VISIBLE);
                return true;
            }
        };
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.draggable_item, parent, false);
        return new MyViewHolder(view);
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {
        holder.position = position;
        holder.textView.setText(mItems.get(position));
        holder.itemHolder.setOnLongClickListener(listener);
        holder.itemHolder.setTag(holder);
    }

    @Override
    public int getItemCount() {
        return mItems.size();
    }

    @Override
    public void onItemDismiss(int position) {
        mItems.remove(position);
        notifyItemRemoved(position);
    }

    @Override
    public void onItemMove(int fromPosition, int toPosition) {
        if (fromPosition < toPosition) {
            for (int i = fromPosition; i < toPosition; i++) {
                Collections.swap(mItems, i, i + 1);
            }
        } else {
            for (int i = fromPosition; i > toPosition; i--) {
                Collections.swap(mItems, i, i - 1);
            }
        }
        notifyItemMoved(fromPosition, toPosition);
        return;
    }

    public void removeItem(int position){
        Log.e("MLA", "removeItem : "+String.valueOf(position) );
        mItems.remove(position);
        this.notifyDataSetChanged();
        Log.e("MLA", "removeItem : "+String.valueOf(getItemCount()) );
    }

    public void mute(int position){
        //TODO : MUTE THE GUY
        Log.e("MLA", "mute position : "+String.valueOf(position) );
    }
}
