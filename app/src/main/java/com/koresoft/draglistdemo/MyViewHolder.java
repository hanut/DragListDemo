package com.koresoft.draglistdemo;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

/**
 * Created by Hanut on 14-03-2017.
 */

class MyViewHolder extends RecyclerView.ViewHolder{
    public int position;
    public RelativeLayout itemHolder;
    public TextView textView;
    public ImageView icon;

    public MyViewHolder(View itemView) {
        super(itemView);
        textView = (TextView) itemView.findViewById(R.id.dl_text_view);
        icon = (ImageView) itemView.findViewById(R.id.drawable);
        itemHolder = (RelativeLayout) itemView.findViewById(R.id.item_holder);
    }
}
