package com.koresoft.draglistdemo;

import android.app.Activity;
import android.content.ClipData;
import android.content.ClipDescription;
import android.content.Context;
import android.graphics.Color;
import android.util.Log;
import android.view.DragEvent;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

/**
 * Created by Hanut on 16-03-2017.
 */

public class MyDragEventListener implements View.OnDragListener {

    Activity activity;
    DragComplete dcHandler;

    public interface DragComplete{
        public void callback(int position,String action);
    }

    public MyDragEventListener(Activity activity,DragComplete dc){
        this.activity = activity;
        this.dcHandler = dc;
    }

    // This is the method that the system calls when it dispatches a drag event to the
    // listener.
    public boolean onDrag(View v, DragEvent event) {

        // Defines a variable to store the action type for the incoming event
        final int action = event.getAction();

        // Handles each of the expected events
        switch(action) {
            case DragEvent.ACTION_DRAG_STARTED:

                // Determines if this View can accept the dragged data
                if (event.getClipDescription().hasMimeType(ClipDescription.MIMETYPE_TEXT_PLAIN)) {
                    // returns true to indicate that the View can accept the dragged data.
                    return true;
                }
                // Returns false. During the current drag and drop operation, this View will
                // not receive events again until ACTION_DRAG_ENDED is sent.
                return false;
            case DragEvent.ACTION_DRAG_ENTERED:
                v.setBackgroundColor(Color.argb(255,255,0,0));
                v.invalidate();
                return true;
            case DragEvent.ACTION_DRAG_LOCATION:
                // Ignore the event
                return true;
            case DragEvent.ACTION_DRAG_EXITED:
                v.setBackgroundColor(Color.TRANSPARENT);
                v.invalidate();
                return true;

            case DragEvent.ACTION_DROP:
                v.setBackgroundColor(Color.TRANSPARENT);
                activity.findViewById(R.id.dragList).setVisibility(View.VISIBLE);
                activity.findViewById(R.id.dropZone).setVisibility(View.GONE);
                // Gets the item containing the dragged data
                ClipData.Item item = event.getClipData().getItemAt(0);
                // Gets the text data from the item.
                int position = Integer.parseInt(item.getText().toString());
                // Displays a message containing the dragged data.
                try{
                    String actionType = ((TextView)v).getText().toString();
                    dcHandler.callback(position, actionType);
                }catch (Exception e){
                    e.printStackTrace();
                }
                return true;

            case DragEvent.ACTION_DRAG_ENDED:
                return true;
            // An unknown action type was received.
            default:
                break;
        }

        return false;
    }
}
